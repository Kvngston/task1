﻿using System;
using System.Collections.Generic;
using System.Text;
using Task1.Models;

namespace Task1.Services
{
    public class PersonService
    {
        private static Person person;

        public PersonService(string fName, string lName, string age)
        {
            person = new Person(firstName:fName, lastName: lName, age: age);
        }


        public static void GetPersonalInfo()
        {
            Console.WriteLine($"Hi, my name is {person.lastName} {person.firstName} and i'm {person.age} years old");
        }
    }
}
