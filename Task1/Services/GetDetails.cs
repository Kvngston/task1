﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Task1
{
    class GetDetails
    {

        public string GetFirstName()
        {

            Console.WriteLine("Enter Your First Name!");
            string firstName = Console.ReadLine();

            if (string.IsNullOrEmpty(firstName))
            {
                throw new Exception("First Name Cannot be blank");
            }

            return firstName;
        }

        public string GetLastName()
        {

            Console.WriteLine("Enter Your Last Name!");
            string lastName = Console.ReadLine();

            if (string.IsNullOrEmpty(lastName))
            {
                throw new Exception("Last Name Cannot be blank");
            }

            return lastName;
        }

        public string GetAge()
        {
            Console.WriteLine("Enter Your Date of Birth in the format YYYY-MM-DD !");
            string input = Console.ReadLine();

            Regex regex = new Regex(@"^\d{4}-\d{2}-\d{2}");
            Match match = regex.Match(input);
            if (match.Success == false)
            {
                throw new Exception("Invalid Date format");
            }

            DateTime dob;
            try
            {
                dob = new DateTime(
                    Convert.ToInt32(input.Split("-")[0]),
                    Convert.ToInt32(input.Split("-")[1]),
                    Convert.ToInt32(input.Split("-")[2])
                    );
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            var today = DateTime.Now;
            if (dob.CompareTo(today) == 1)
            {
                throw new Exception("Invalid Date, date cannot be in the future");
            }
            return (today.Year - dob.Year).ToString();
        }

    }
}
