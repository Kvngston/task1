﻿using System;
using System.Text.RegularExpressions;
using Task1.Services;
namespace Task1.Services
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hey There!");
            GetDetails details = new GetDetails();

            try
            {
                string firstName = details.GetFirstName();
                string lastName = details.GetLastName();
                string age = details.GetAge();

                PersonService personService = new PersonService(firstName, lastName, age);
                PersonService.GetPersonalInfo();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            
        }
    }
}
