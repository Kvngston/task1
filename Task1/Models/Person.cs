﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task1.Models
{
    public class Person
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string age { get; set; }

        public Person()
        {
           
        }
        public Person(string firstName, string lastName, string age)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }
    }
}
